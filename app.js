const Discord = require('discord.js');
const client = new Discord.Client();


client.on("ready", () => {
    
    require('./router');
    console.log(`Logged in as ${client.user.tag}`);
    client.user.setActivity('t!info', { type: 'WATCHING' })
});

client.on("message", (msg) => {
    if (msg.content.indexOf("t!id") == 0) {
        msg.channel.send('`' + msg.channel.id+'`')
    } else if (msg.content.indexOf("t!info") == 0) {
            msg.channel.send("Ce bot permet de gérer les tableau de combat. \nInterface : http://62.4.21.196/ (Nom de domaine et HTTPS arriveront bientôt\nAstuce : Faites t!id pour trouver l'id d'un salon discord")
    }
    client.user.setActivity('t!info', { type: 'WATCHING' })
})

client.login('token');

exports.getDiscordClient = () => {
    return client;
}