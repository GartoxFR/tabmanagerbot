const { getDiscordClient } = require('./app');
const client = getDiscordClient();
const express = require('express');
const app = express();
const  bodyParser = require('body-parser')
app.use(bodyParser.json())

app.all("/*", function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
  });
app.post('/sendTable', (req, res) => {
    client.channels.fetch(req.body.channel).then(channel => {
        let message = 
 `\`\`\`
--------------
|  Monstres  |
--------------
        
`;

        for(monster of req.body.monsters) {
            message += (monster.name + ' : ' + monster.hp + ' ' + (monster.provocationPlayer == '' ? '' : 'Provocation par ' + monster.provocationPlayer) + 
            (monster.annotation == '' ? '' : '(' + monster.annotation + ')') + '\n');
        }
        message +=
    `\n------------
| Joueurs  |
------------

`
        for(let i = 0 ; i < req.body.players.length ; i++) {
            let player = req.body.players[i]
            let n = i + 1;
            message += n + '. ' + player.name + ' ' + (player.annotation == '' ? '' : '(' + player.annotation + ')') + '\n';
        }
        message += "```"
        channel.send(message);
    }).catch(err => {
        console.log(err);
    });
    res.status(200)
    res.end();
})

app.post('/init', (req, res) => {

    client.channels.fetch(req.body.channel).then(channel => channel.send("Un nouveau tableau a bien été créé dans ce channel")).catch(err=> console.log(err));
    
    res.status(200)
    res.end();
})

app.listen(8080, () => {
    console.log("Express listening...");
    
})